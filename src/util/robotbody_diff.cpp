#include "ros/ros.h"
#include <geometry_msgs/PoseStamped.h>

#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames_io.hpp>
#include <kdl_conversions/kdl_msg.h>

KDL::Frame Frobot;
KDL::Frame Foptitrack;
KDL::Twist Fdiff;

void robotPose(const geometry_msgs::PoseStamped& msg)
{
    // ROS_INFO_STREAM("Current robot pose:" << msg.pose.position.x << '\t' <<
    //                                         msg.pose.position.y << '\t' <<
    //                                         msg.pose.position.z << '\t' <<
    //                                         msg.pose.orientation.x << '\t' <<
    //                                         msg.pose.orientation.y << '\t' <<
    //                                         msg.pose.orientation.z << '\n');

    Frobot.p = KDL::Vector(msg.pose.position.x, 
                            msg.pose.position.y, 
                            msg.pose.position.z);
    Frobot.M = KDL::Rotation::Quaternion(msg.pose.orientation.x,
                                        msg.pose.orientation.y,
                                        msg.pose.orientation.z,
                                        msg.pose.orientation.w);

    // Must be shifted -10.5 cm along z of EE
    KDL::Vector p_shift = Frobot.M * KDL::Vector(0,0,-0.107);
    Frobot.p += p_shift;
}

void optitrackPose(const geometry_msgs::PoseStamped& msg)
{
    // ROS_INFO_STREAM("Current optitrack pose:" << msg.pose.position.x << '\t' <<
    //                                         msg.pose.position.y << '\t' <<
    //                                         msg.pose.position.z << '\t' <<
    //                                         msg.pose.orientation.x << '\t' <<
    //                                         msg.pose.orientation.y << '\t' <<
    //                                         msg.pose.orientation.z << '\n');
    Foptitrack.p = KDL::Vector(msg.pose.position.x, 
                            msg.pose.position.y, 
                            msg.pose.position.z);
    Foptitrack.M = KDL::Rotation::Quaternion(msg.pose.orientation.x,
                                        msg.pose.orientation.y,
                                        msg.pose.orientation.z,
                                        msg.pose.orientation.w);
    Fdiff = diff(Frobot,Foptitrack);
    ROS_INFO_STREAM("Frame difference pose:" << Fdiff.rot << '\t' <<
                                            Fdiff.vel << '\n');

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "robotbody_diff");
  ros::NodeHandle n;

  ros::Subscriber robotsub = n.subscribe("/panda_mover/panda_pose", 100, robotPose);
  ros::Subscriber optitracksub = n.subscribe("/vrpn_client_node/RobotBody/pose", 100, optitrackPose);

  ros::spin();

  return 0;
}