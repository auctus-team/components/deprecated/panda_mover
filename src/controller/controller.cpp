#include "panda_mover/controller/controller.h"
#include <ros/console.h>

using namespace KDL;
using namespace std;

namespace Controller{
bool Controller::init(ros::NodeHandle& node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qd_init) 
{
    ROS_WARN("Using panda_mover controller !!! ");
    //--------------------------------------
    // INITIALIZE PUBLISHERS
    //--------------------------------------
    init_publishers(node_handle);
    
    //--------------------------------------
    // LOAD PARAMETERS
    //--------------------------------------
    load_parameters();
    if (!node_handle.getParam("/sim", sim))
    {
        ROS_ERROR_STREAM("Could not read parameter sim");
        return false;
    }
    if (!node_handle.getParam("/test_constraints", test_constraints))
    {
        ROS_ERROR_STREAM("Could not read parameter test_constraints");
        return false;
    }
    
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    if (!load_robot(node_handle))
        return false;        
    
    //--------------------------------------
    // INITIALIZE VARIABLES
    //--------------------------------------  
    joint_velocity_out_.resize(dof);
    joint_torque_out_.resize(dof);
    control_action.resize(dof);
    q_kdl.resize(chain.getNrOfJoints());
    qd_kdl.resize(chain.getNrOfJoints());
    qd_prev_.resize(chain.getNrOfJoints());
    qd_prev_.setZero();
    qdd_curr_.resize(chain.getNrOfJoints());
    tau_kdl.resize(chain.getNrOfJoints());

    J_.resize(chain.getNrOfJoints());
    M_.resize(chain.getNrOfJoints());
    H_.resize(number_of_variables,number_of_variables);
    g_.resize(number_of_variables);
    lb_.resize(number_of_variables);
    ub_.resize(number_of_variables);
    A_.resize(number_of_constraints_,number_of_variables);
    lbA_.resize(number_of_constraints_);
    ubA_.resize(number_of_constraints_);
    a_.resize(dof,number_of_variables);
    nonLinearTerms_.resize(number_of_variables);
    gravity_kdl.resize(number_of_variables);
    coriolis_kdl.resize(number_of_variables);    

    publish_traj = false;
    play_traj_ = false;
    selection_matrix_ = Eigen::MatrixXd::Identity(6,6);
    
    //--------------------------------------
    // QPOASES
    //--------------------------------------
    qpoases_solver_.reset(new qpOASES::SQProblem(number_of_variables,number_of_constraints_,qpOASES::HST_POSDEF));

    // QPOases options
    qpOASES::Options options;
    // This options enables regularisation (required) and disable
    // some checks to be very fast !
    // options.setToDefault();
    options.setToMPC(); // setToReliable() // setToDefault()
    options.enableRegularisation = qpOASES::BT_FALSE; // since we specify the type of Hessian matrix, we do not need automatic regularisation
    options.enableEqualities = qpOASES::BT_TRUE; // Specifies whether equalities shall be  always treated as active constraints.
    qpoases_solver_->setOptions( options );
    qpoases_solver_->setPrintLevel(qpOASES::PL_NONE); // PL_HIGH for full output, PL_NONE for... none
    
    q_in.q.data = q_init;
    q_in.qdot.data = qd_init;
    fksolver_->JntToCart(q_in.q, X_curr_);

    // Initialize controller inputs
    integral_error_.setZero();
    Xd_traj_.Zero();
    Xdd_traj_.Zero();
    X_traj_ = X_curr_;
    
    //--------------------------------------
    // BUILD TRAJECTORY 
    //--------------------------------------
    ROS_INFO_STREAM(" Trajectory loaded from " << trajectory_file);
    std::string csv_file_name = trajectory_file;
    trajectory.Load(csv_file_name);
    build_trajectory(X_curr_);
    ROS_INFO_STREAM(" Trajectory computed " );
    ros::param::set("panda_mover/Traj_duration", trajectory.Duration());
    ros::param::set("panda_mover/Init_duration", trajectory.ResetDuration());

    traj_properties_.play_traj_ = false;
    traj_properties_.gain_tunning_ = false;
    traj_properties_.jogging_ = false;
    
    return true;
}

Eigen::VectorXd Controller::update(Eigen::VectorXd q, Eigen::VectorXd qd, const ros::Duration& period, std::string &control_level)
{        
    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    ROS_INFO_ONCE("robot_state");
    q_in.q.data = q;
    q_in.qdot.data = qd;
    chainjacsolver_->JntToJac(q_in.q, J_);
    fksolver_->JntToCart(q_in.q, X_curr_);
    fksolvervel_->JntToCart(q_in, Xd_curr_);
    dynModelSolver_->JntToMass(q_in.q, M_);
    dynModelSolver_->JntToGravity(q_in.q, gravity_kdl);
    dynModelSolver_->JntToCoriolis(q_in.q,q_in.qdot, coriolis_kdl);
    chainjnttojacdotsolver_->JntToJacDot(q_in,Jdqd_kdl,chain.getNrOfJoints());

    //--------------------------------------
    // INCREMENT TRAJECTORY
    //--------------------------------------
    ROS_INFO_ONCE("increment_trajectory");
    increment_trajectory();

    //--------------------------------------
    // QP SOLVER  
    //--------------------------------------
    ROS_INFO_ONCE("qp_solver");
    control_level = control_level_;
    if (control_level == "velocity"){
        control_action = joint_velocity_solver(q, qd);
    }else if (control_level == "torque"){
        control_action = joint_torque_solver(q, qd);
    }

    //--------------------------------------
    // PUBLISHING
    //--------------------------------------
    ROS_INFO_ONCE("do_publishing");
    do_publishing();
    
    return control_action;  
}

void Controller::increment_trajectory(){
    tf::poseKDLToMsg(X_curr_, traj_properties_.X_curr_);

    if (!manual_movement){
        // Update the trajectory
        if (!must_rebuild){
            trajectory.updateTrajectory(traj_properties_, TIME_DT, manual_movement);
            if (traj_properties_.move_)
                traj_properties_.move_ = false;
            X_traj_ = trajectory.Pos();
            Xd_traj_ = trajectory.Vel();
            Xdd_traj_ = trajectory.Acc(); 
        } else {
            ROS_WARN_THROTTLE(1,"Rebuild the trajectory.");
        }

        // Check if the next frame is similar to the current frame (Is it safe to proceed?)
        if (traj_properties_.play_traj_ && (X_curr_.p - X_traj_.p).Norm() > 0.025){
            X_traj_ = X_curr_;
            Xd_traj_.Zero();
            Xdd_traj_.Zero();
            must_rebuild = true;
        }

        //--------------------------------------
        // GRIPPER ACTIONS
        //--------------------------------------
        if (traj_properties_.play_traj_)
        {        
            if (traj_gripper_counter < trajectory.gripper_action_list.size() &&
                trajectory.CurrentTime() >= trajectory.gripper_action_list[traj_gripper_counter].start_time - 0.001)
            {
                // Apply gripper action
                if (trajectory.gripper_action_list[traj_gripper_counter].type.compare("GRIPPER_HOMING")==0){
                    franka_gripper::HomingGoal goal;
                    franka_gripper_homing_action_client->sendGoal(goal);
                    ROS_INFO_STREAM("GRIPPER_HOMING");
                }else if (trajectory.gripper_action_list[traj_gripper_counter].type.compare("GRIPPER_STOP")==0){
                    franka_gripper::StopGoal goal;
                    franka_gripper_stop_action_client->sendGoal(goal);
                    ROS_INFO_STREAM("GRIPPER_STOP");
                }else if (trajectory.gripper_action_list[traj_gripper_counter].type.compare("GRIPPER_GRASP")==0){
                    franka_gripper::GraspGoal goal;
                    goal.width = trajectory.gripper_action_list[traj_gripper_counter].options[0];
                    goal.speed = trajectory.gripper_action_list[traj_gripper_counter].options[1];
                    goal.speed = trajectory.gripper_action_list[traj_gripper_counter].options[2];
                    goal.epsilon.inner = trajectory.gripper_action_list[traj_gripper_counter].options[3];
                    goal.epsilon.outer = trajectory.gripper_action_list[traj_gripper_counter].options[4];
                    franka_gripper_grasp_action_client->sendGoal(goal);
                    ROS_INFO_STREAM("GRIPPER_GRASP");
                }else if (trajectory.gripper_action_list[traj_gripper_counter].type.compare("GRIPPER_MOVE")==0){
                    franka_gripper::MoveGoal goal;
                    goal.width = trajectory.gripper_action_list[traj_gripper_counter].options[0];
                    goal.speed = trajectory.gripper_action_list[traj_gripper_counter].options[1];
                    franka_gripper_move_action_client->sendGoal(goal);
                    ROS_INFO_STREAM("GRIPPER_MOVE");
                }            
                // Increment gripper counter
                traj_gripper_counter++;
            }        
        } 
        else 
        {
            traj_gripper_counter = 0;
        }
    }
}


Eigen::VectorXd Controller::joint_velocity_solver(Eigen::VectorXd &q, Eigen::VectorXd &qd){
    //--------------------------------------
    // CONTROL PROBLEM
    //--------------------------------------
    double horizon_dt = HORIZON_DT;

    // Proportionnal controller
    ROS_INFO_ONCE("proportional_controller");
    X_err_ = diff(X_curr_, X_traj_); 
    tf::twistKDLToEigen(X_err_, x_err);
    tf::twistKDLToEigen(Xd_traj_, xd_traj);
    xd_des_ = p_gains_.cwiseProduct(x_err) + xd_traj;
    
    //--------------------------------------
    // SOLVE QP
    //--------------------------------------
    ROS_INFO_ONCE("joint_velocity_qp_solver");   
    H_ =  2.0 * regularisation_weight_ * Eigen::MatrixXd::Identity(7,7);
    g_ = -2.0 * regularisation_weight_ * (p_gains_qd_.cwiseProduct(q_mean_ - q));
    H_ +=  2.0 *  J_.data.transpose() * J_.data;
    g_ += -2.0 *  J_.data.transpose() * xd_des_;        
    lb_ = -qd_max_;
    ub_ = qd_max_;    
    A_.block(0,0,7,7) = horizon_dt * Eigen::MatrixXd::Identity(7,7);    
    ubA_.segment(0,7) = ul.data - q;
    lbA_.segment(0,7) = ll.data - q;    
    
    // number of allowed compute steps
    int nWSR = 1e6;

    // Let's compute !
    qpOASES::returnValue ret;
    static bool qpoases_initialized = false;

    if(!qpoases_initialized){
        // Initialise the problem, once it has found a solution, we can hotstart
        ret = qpoases_solver_->init(H_.data(),g_.data(),A_.data(),lb_.data(),ub_.data(),lbA_.data(),ubA_.data(),nWSR);

        // Keep init if it didn't work
        if(ret == qpOASES::SUCCESSFUL_RETURN)
        qpoases_initialized = true;
    }
    else{
        // Otherwise let's reuse the previous solution to find a solution faster
        ret = qpoases_solver_->hotstart(H_.data(),g_.data(),A_.data(),lb_.data(),ub_.data(),lbA_.data(),ubA_.data(),nWSR);

        if(ret != qpOASES::SUCCESSFUL_RETURN)
        qpoases_initialized = false;
    }
    
    // Zero velocity if no solution found
    joint_velocity_out_.setZero();

    if(ret == qpOASES::SUCCESSFUL_RETURN){
        qpoases_solver_->getPrimalSolution(joint_velocity_out_.data());
        qp_solver_failed = false;        
        
        // Compute current acceleration
        qdd_curr_ = (joint_velocity_out_ - qd_prev_) * (1 / TIME_DT);
        qd_prev_ = joint_velocity_out_;
    }else{
        ROS_WARN_THROTTLE(1, "QPOases failed! Sending zero velocity");
        qp_solver_failed = true;
        
        // Compute current acceleration
        qdd_curr_ = (qd - qd_prev_) * (1 / TIME_DT);
        qd_prev_ = qd;
    }

    return joint_velocity_out_;
}

Eigen::VectorXd Controller::joint_torque_solver(Eigen::VectorXd &q, Eigen::VectorXd &qd){
    //--------------------------------------
    // CONTROL PROBLEM
    //--------------------------------------
    double horizon_dt = HORIZON_DT;
    Xd_curr_twist_ = Xd_curr_.GetTwist();

    // Change reference frame to end effector 
    R_ee_base = toEigenMatrix(X_curr_.M).transpose();
    RR.setZero();
    RR.block(0,0,3,3) = R_ee_base;
    RR.block(3,3,3,3) = R_ee_base;

    // Compute error
    if (manual_movement){
        X_err_ = diffFrame(X_curr_, X_traj_);
        X_err_.rot = error_multiplier * X_err_.rot;
    } else
        X_err_ = diff(X_curr_, X_traj_); 
    tf::twistKDLToEigen(X_err_, x_err);   

    // Saturate the integral term
    for(unsigned int i=0; i<3; ++i ){
        if (i_gains_(i) > 0){
            integral_error_(i) += x_err(i) * TIME_DT;
            if(integral_error_(i) >0)
                integral_error_(i) = std::min(integral_pos_saturation_ / i_gains_(i), integral_error_(i));
            else
                integral_error_(i) = std::max(-integral_pos_saturation_ / i_gains_(i), integral_error_(i));
            }
        else
            integral_error_(i) = 0;
    }
    for(unsigned int i=3; i<6; ++i ){
        if (i_gains_(i) > 0){
            integral_error_(i) += x_err(i) * TIME_DT;
            if(integral_error_(i) >0)
                integral_error_(i) = std::min(integral_rot_saturation_ / i_gains_(i), integral_error_(i));
            else
                integral_error_(i) = std::max(-integral_rot_saturation_ / i_gains_(i), integral_error_(i));
        }
        else
            integral_error_(i) = 0;
    }

    // PID controller
    tf::twistKDLToEigen(Xd_curr_twist_, xd_curr_);
    tf::twistKDLToEigen(Xdd_traj_ ,xdd_traj);
    xdd_des_ = xdd_traj + p_gains_.cwiseProduct(x_err) + i_gains_.cwiseProduct(integral_error_) - d_gains_.cwiseProduct(xd_curr_);

    // Set joint q2 limits when appropriate
    if (q(1) > q2_lb)
        ll.data(1) = q2_lb; 
    if (q(1) < q2_ub)
        ul.data(1) = q2_ub;    
    
    //--------------------------------------
    // SOLVE QP
    //--------------------------------------
    ROS_INFO_ONCE("joint_torque_qp_solver");
    J = J_.data;
    M = M_.data;
    gravity = gravity_kdl.data;
    coriolis = coriolis_kdl.data;
    tf::twistKDLToEigen(Jdqd_kdl,jdot_qdot_);
    nonLinearTerms_ = M.inverse() * (coriolis + gravity);

    // Change reference frame to end effector
    if (manual_movement){
        J = RR * J;
        jdot_qdot_ = RR * jdot_qdot_;
        xdd_des_ = RR * xdd_des_;
    }
    
    H_ = 2.0 * regularisation_weight_ * Eigen::MatrixXd::Identity(7,7);
    g_ = -2.0 * (regularisation_weight_ * gravity );

    a_.noalias() = J * M.inverse();
    b_.noalias() = -a_ * (coriolis + gravity) + jdot_qdot_ - xdd_des_;
    H_ += 2.0 * a_.transpose() * selection_matrix_ * a_;
    g_ += 2.0 * a_.transpose() * selection_matrix_ * b_; 

    ub_ =  torque_max_;
    lb_ = -torque_max_;  

    A_.block(0, 0, 7, 7) = M.inverse();
    lbA_.block(0, 0, dof, 1) =
        ((qd_min_ - qd) / horizon_dt + nonLinearTerms_)
            .cwiseMax(2 * (ll.data - q - qd * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms_);
    ubA_.block(0, 0, dof, 1) =
        ((qd_max_ - qd) / horizon_dt + nonLinearTerms_)
            .cwiseMin(2 * (ul.data - q - qd * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms_);

    // number of allowed compute steps
    int nWSR = 1e6;

    // Let's compute !
    qpOASES::returnValue ret;
    static bool qpoases_initialized = false;

    if(!qpoases_initialized){
        // Initialise the problem, once it has found a solution, we can hotstart
        ret = qpoases_solver_->init(H_.data(),g_.data(),A_.data(),lb_.data(),ub_.data(),lbA_.data(),ubA_.data(),nWSR);

        // Keep init if it didn't work
        if(ret == qpOASES::SUCCESSFUL_RETURN)
        qpoases_initialized = true;
    }
    else{
        // Otherwise let's reuse the previous solution to find a solution faster
        ret = qpoases_solver_->hotstart(H_.data(),g_.data(),A_.data(),lb_.data(),ub_.data(),lbA_.data(),ubA_.data(),nWSR);

        if(ret != qpOASES::SUCCESSFUL_RETURN)
        qpoases_initialized = false;
    }
    
    // Zero velocity if no solution found
    joint_torque_out_.setZero();

    if (ret == qpOASES::SUCCESSFUL_RETURN)
        qpoases_solver_->getPrimalSolution(joint_torque_out_.data());
    else
        ROS_WARN_THROTTLE(1, "QPOases failed! Sending zero torque");

    qpoases_solver_->getConstraints(constraints_);
    qpoases_solver_->getBounds(bounds_);

    activated_cons_.resize(constraints_.getNC());
    for (int i = 0; i < constraints_.getNC(); ++i)
        activated_cons_[i] = 0;
      qpOASES::Indexlist* cons_list = constraints_.getActive();

    for (int i = 0; i < cons_list->getLength(); ++i)
    {
        activated_cons_[cons_list->getNumber(i)] = 1;
    }
    activated_bounds_.resize(bounds_.getNBV());
    for (int i = 0; i < bounds_.getNBV(); ++i)
        activated_bounds_[i] = 0;

    qpOASES::Indexlist* bound_list = bounds_.getFixed();
    for (int i = 0; i < bound_list->getLength(); ++i)
    {
        activated_bounds_[bound_list->getNumber(i)] = 1;
    }

    if (virtual_walls){
        // Virtual wall tube with orientation
        KDL::Vector u = trajectory.wireloop_frame.p - X_curr_.p;   
        double radius = 0.02;
        double u_norm = u.Norm(); 
        KDL::Vector x_des = trajectory.wireloop_frame.M.UnitX();
        KDL::Vector x_curr = X_curr_.M.UnitX(); 
        KDL::Vector z_curr = X_curr_.M.UnitZ(); 
        double cos_angle = KDL::dot(x_des, x_curr);
        double cos_angle_limit = 0.4;
        double z_min = 0.42;

        // Near path
        if (u_norm<radius && cos_angle > cos_angle_limit && X_curr_.p(2) > z_min) near_path = true;

        // Leaves path for whatever reason
        if (near_path && u_norm>(radius+0.01)) near_path = false;

        // Virtual wall constraints
        double f_mag_x = 0;
        double f_mag_y = 0;
        double f_mag_z = 0;
        double t_mag_x = 0;
        double t_mag_y = 0;
        double t_mag_z = 0;

        // Start/End Position
        double wp = 5000;
        double wd = 5000;
        if (X_curr_.p(2) < z_min && !traj_properties_.play_traj_ && near_path)
        {
            f_mag_z += wp*(z_min - X_curr_.p(2)) - sqrt(2*wd) * xd_curr_(2);
            ROS_INFO_STREAM(" Virtual wall start/end position: " << f_mag_x << '\t' << f_mag_y << '\t' << f_mag_z);
        }

        // Wireloop Position
        if (u_norm > radius && near_path)
        {
            u = (u_norm - radius) * u / u_norm;
            f_mag_x += wp*(u(0)) - sqrt(2*wd) * xd_curr_(0);
            f_mag_y += wp*(u(1)) - sqrt(2*wd) * xd_curr_(1);
            f_mag_z += wp*(u(2)) - sqrt(2*wd) * xd_curr_(2);
            ROS_INFO_STREAM(" Virtual wall position: " << f_mag_x << '\t' << f_mag_y << '\t' << f_mag_z);
        }
        // // Wireloop Orientation
        // if (cos_angle < cos_angle_limit && near_path)
        // {
        //     double dir_sign = 0;
        //     if ((x_curr(0) * x_des(1) - x_curr(1) * x_des(0)) > 0)
        //         dir_sign = 1.0;
        //     else 
        //         dir_sign = -1.0;
        //     t_mag_x = wp*(dir_sign * (cos_angle_limit - cos_angle) * z_curr(0)) - sqrt(2*wd) * xd_curr_(3);
        //     t_mag_y = wp*(dir_sign * (cos_angle_limit - cos_angle) * z_curr(1)) - sqrt(2*wd) * xd_curr_(4);
        //     t_mag_z = wp*(dir_sign * (cos_angle_limit - cos_angle) * z_curr(2)) - sqrt(2*wd) * xd_curr_(5);
        //     ROS_INFO_STREAM(" Virtual wall orientation: " << t_mag_x << '\t' << t_mag_y << '\t' << t_mag_z);
        // }
        Eigen::VectorXd w_u(6);
        w_u << f_mag_x, f_mag_y, f_mag_z, t_mag_x, t_mag_y, t_mag_z;
        joint_torque_out_ += J.block(0,0,6,7).transpose() * w_u;
    }

    // // Joint angle constraints    
    // if (q(6) > q7_lb && q(6) < q7_ub){
    //     if (q(6) < q7_lb){
    //         joint_torque_out_(6) += 10*(q7_lb - q(6));
    //         ROS_INFO_STREAM(" Virtual wall q7 " << q(6));
    //     }else if (q(6) > q7_ub){
    //         joint_torque_out_(6) += 10*(q7_ub - q(6));
    //         ROS_INFO_STREAM(" Virtual wall q7"  << q(6));
    //     }
    // }

    if (!sim)
        joint_torque_out_ -= gravity;     

    return joint_torque_out_;
}

void Controller::build_trajectory(KDL::Frame X_curr_)
{
    trajectory.SetAccMax(amax_traj_);
    trajectory.Build(X_curr_, false);
    must_rebuild = false;
    publish_trajectory();

    // Polyline
    if (trajectory.polyline.size() > 0){
        ROS_INFO_STREAM(" Polyline segments: " );
        for (int i=0; i<trajectory.polyline.size(); i++){
            ROS_INFO_STREAM(trajectory.polyline[i].movetype << '\t' << 
            trajectory.polyline[i].start_frame << '\t' << trajectory.polyline[i].end_frame);
        }
    }
}

void Controller::publish_trajectory()
{
    panda_traj::PublishTraj publish_traj_;
    publish_traj_ = trajectory.publishTrajectory();

    nav_msgs::Path path_ros;
    path_ros.poses = publish_traj_.path_ros_.poses;
    path_ros.header.frame_id = root_link_;
    path_ros.header.stamp = ros::Time::now();
    geometry_msgs::PoseArray pose_array;
    pose_array.poses = publish_traj_.pose_array_.poses;
    pose_array.header.frame_id = root_link_;
    pose_array.header.stamp = ros::Time::now();

    if (pose_array_publisher.trylock())
    {
        pose_array_publisher.msg_.header.stamp = ros::Time::now();
        pose_array_publisher.msg_.header.frame_id = root_link_;
        pose_array_publisher.msg_ = pose_array;
        pose_array_publisher.unlockAndPublish();
    }
    if (path_publisher.trylock())
    {
        path_publisher.msg_.header.stamp = ros::Time::now();
        path_publisher.msg_.header.frame_id = root_link_;
        path_publisher.msg_ = path_ros;
        path_publisher.unlockAndPublish();
    }

    // Polyline frames
    geometry_msgs::PoseArray polyline_pose_array;
    polyline_pose_array.header.frame_id = root_link_;
    polyline_pose_array.header.stamp = ros::Time::now();
    if (trajectory.polyline.size()>0){
        geometry_msgs::Pose pose;            
        tf::poseKDLToMsg(trajectory.polyline[0].start_frame,pose);
        polyline_pose_array.poses.push_back(pose);
    }
    for (int i=0; i<trajectory.polyline.size(); i++)
    {
        geometry_msgs::Pose pose;            
        tf::poseKDLToMsg(trajectory.polyline[i].end_frame,pose);
        polyline_pose_array.poses.push_back(pose);
    }

    if (polyline_pose_array_publisher.trylock()){
        polyline_pose_array_publisher.msg_.header.stamp = ros::Time::now();
        polyline_pose_array_publisher.msg_.header.frame_id = root_link_;
        polyline_pose_array_publisher.msg_= polyline_pose_array;
        polyline_pose_array_publisher.unlockAndPublish();
    }

    // Polyline intersection frames
    geometry_msgs::PoseArray polyline_inter_pose_array;
    polyline_inter_pose_array.header.frame_id = root_link_;
    polyline_inter_pose_array.header.stamp = ros::Time::now();
    for (int i=0; i<trajectory.intersection_frames.size(); i++)
    {
        geometry_msgs::Pose pose;            
        tf::poseKDLToMsg(trajectory.intersection_frames[i],pose);
        polyline_inter_pose_array.poses.push_back(pose);
    }

    if (polyline_inter_pose_array_publisher.trylock()){
        polyline_inter_pose_array_publisher.msg_.header.stamp = ros::Time::now();
        polyline_inter_pose_array_publisher.msg_.header.frame_id = root_link_;
        polyline_inter_pose_array_publisher.msg_= polyline_inter_pose_array;
        polyline_inter_pose_array_publisher.unlockAndPublish();
    }
    ROS_INFO_STREAM(" Trajectory published ");
}

void Controller::init_publishers(ros::NodeHandle& node_handle){
    //Realtime safe publishers
    pose_array_publisher.init(node_handle, "Pose_array", 1);
    polyline_pose_array_publisher.init(node_handle, "Polyline_pose_array", 1);
    polyline_inter_pose_array_publisher.init(node_handle, "Polyline_intersection_pose_array", 1);
    path_publisher.init(node_handle, "Ros_Path", 1);    
    panda_rundata_publisher.init(node_handle, "panda_rundata", 1);
    panda_pose_publisher.init(node_handle, "panda_pose", 1);
    trajectory_pose_publisher.init(node_handle, "trajectory_pose", 1);
    wireloop_pose_publisher.init(node_handle, "wireloop_pose", 1);

    // Gripper publishers
    franka_gripper_homing_action_client = new actionlib::SimpleActionClient<franka_gripper::HomingAction>("/franka_gripper/homing", true);
    franka_gripper_move_action_client = new actionlib::SimpleActionClient<franka_gripper::MoveAction>("/franka_gripper/move", true);
    franka_gripper_stop_action_client = new actionlib::SimpleActionClient<franka_gripper::StopAction>("/franka_gripper/stop", true);
    franka_gripper_grasp_action_client = new actionlib::SimpleActionClient<franka_gripper::GraspAction>("/franka_gripper/grasp", true);
//     franka_gripper_homing_action_client->waitForServer();
//     franka_gripper_move_action_client->waitForServer();
//     franka_gripper_stop_action_client->waitForServer();
//     franka_gripper_grasp_action_client->waitForServer();
}

void Controller::load_parameters(){    
    ROS_INFO_STREAM ( "------------- Loading parameters -------------" );
    qd_min_.resize(7);
    getRosParam("/panda_mover/qd_min_",qd_min_);
    qd_max_.resize(7);
    getRosParam("/panda_mover/qd_max_",qd_max_);
    p_gains_.resize(6);
    getRosParam("/panda_mover/p_gains_",p_gains_);
    d_gains_.resize(6);
    getRosParam("/panda_mover/d_gains_",d_gains_);
    i_gains_.resize(6);
    getRosParam("/panda_mover/i_gains_",i_gains_);
    p_gains_qd_.resize(7);
    getRosParam("/panda_mover/p_gains_qd_",p_gains_qd_);
    torque_max_.resize(7);
    getRosParam("/panda_mover/torque_max_",torque_max_);
    qdd_max_.resize(7);
    getRosParam("/panda_mover/qdd_max_",qdd_max_);    
    qddd_max_.resize(7);
    getRosParam("/panda_mover/qddd_max_",qddd_max_);
    q_mean_.resize(7);
    getRosParam("/panda_mover/q_mean_",q_mean_);
    getRosParam("/panda_mover/amax_traj_",amax_traj_);
    getRosParam("/panda_mover/regularisation_weight_",regularisation_weight_);
    getRosParam("/panda_mover/trajectory_file",trajectory_file);
    getRosParam("/panda_mover/root_link_",root_link_);
    getRosParam("/panda_mover/tip_link_",tip_link_);
    getRosParam("/telemanipulation",telemanipulation);
    getRosParam("/panda_mover/control_level",control_level_);
    getRosParam("/panda_mover/integral_pos_saturation_",integral_pos_saturation_);
    getRosParam("/panda_mover/integral_rot_saturation_",integral_rot_saturation_);
    getRosParam("/panda_mover/error_multiplier",error_multiplier);
    ROS_INFO_STREAM ( "------------- Parameters Loaded -------------" );
}    

bool Controller::load_robot(ros::NodeHandle &node_handle)
{
    service = node_handle.advertiseService("updateUI", &Controller::updateUI, this);
    // get robot descritpion
    double timeout;
    node_handle.param("timeout", timeout, 0.005);
    std::string urdf_param;
    node_handle.param("urdf_param", urdf_param, std::string("/robot_description"));
    double eps = 1e-5;
    ik_solver.reset(new TRAC_IK::TRAC_IK(root_link_, tip_link_, urdf_param, timeout, eps));
    bool valid = ik_solver->getKDLChain(chain);

    if (!valid)
    {
        ROS_ERROR_STREAM("There was no valid KDL chain found");
        return false;
    }
    valid = ik_solver->getKDLLimits(ll, ul);
    if (!valid)
    {
        ROS_ERROR_STREAM("There were no valid KDL joint limits found");
        return false;
    }

    assert(chain.getNrOfJoints() == ll.data.size());
    assert(chain.getNrOfJoints() == ul.data.size());
    q7_lb = ll(6);
    q7_ub = ul(6);

    if (chain.getNrOfSegments() == 0)
        ROS_WARN("KDL chain empty !");

    ROS_INFO_STREAM("  Chain has " << chain.getNrOfJoints() << " joints");
    ROS_INFO_STREAM("  Chain has " << chain.getNrOfSegments() << " segments");

    for (unsigned int i = 0; i < chain.getNrOfSegments(); ++i)
        ROS_INFO_STREAM("    " << chain.getSegment(i).getName());

    fksolver_.reset(new KDL::ChainFkSolverPos_recursive(chain));
    fksolvervel_.reset(new KDL::ChainFkSolverVel_recursive(chain));
    chainjacsolver_.reset(new KDL::ChainJntToJacSolver(chain));
    chainjnttojacdotsolver_.reset(new KDL::ChainJntToJacDotSolver(chain));
    dynModelSolver_.reset(new KDL::ChainDynParam(chain, KDL::Vector(0., 0., -9.81)));
    dof = chain.getNrOfJoints();
    number_of_variables = dof;
    number_of_constraints_ = dof;
    ROS_INFO_STREAM("Number of variables : " << number_of_variables);
    ROS_INFO_STREAM("Number of constraints : " << number_of_constraints_);
    panda_rundata_publisher.msg_.joint_command_.velocity.resize(dof);
    panda_rundata_publisher.msg_.joint_command_.position.resize(dof);
    panda_rundata_publisher.msg_.joint_command_.effort.resize(dof);

    return true;
}

void Controller::do_publishing(){
    // Publishing
    tf::poseKDLToMsg(trajectory.wireloop_frame,X_wireloop_msg_);
    tf::poseKDLToMsg(X_curr_,X_curr_msg_);
    tf::poseKDLToMsg(X_traj_,X_traj_msg_);
    tf::twistKDLToMsg(X_err_,X_err_msg_);
    tf::twistKDLToMsg(Xd_traj_,Xd_msg_);
    tf::twistKDLToMsg(Xdd_traj_,Xdd_msg_);
    tf::twistEigenToMsg(xd_des_,xd_des_msg_);
    tf::twistEigenToMsg(xdd_des_,xdd_des_msg_);
    tf::twistEigenToMsg(integral_error_,integral_error_msg_);
    if (panda_rundata_publisher.trylock()){
        panda_rundata_publisher.msg_.header.stamp = ros::Time::now();
        panda_rundata_publisher.msg_.header.frame_id = root_link_;
        panda_rundata_publisher.msg_.qp_failed = qp_solver_failed;
        panda_rundata_publisher.msg_.X_curr = X_curr_msg_;
        panda_rundata_publisher.msg_.X_traj = X_traj_msg_;
        panda_rundata_publisher.msg_.Xd_traj = Xd_msg_;
        panda_rundata_publisher.msg_.Xdd_traj = Xdd_msg_;
        panda_rundata_publisher.msg_.X_err = X_err_msg_;
        panda_rundata_publisher.msg_.Xd_control = xd_des_msg_;   
        panda_rundata_publisher.msg_.integral_error = integral_error_msg_;   
        panda_rundata_publisher.msg_.Xdd_control = xdd_des_msg_;   
        panda_rundata_publisher.msg_.percent_completion = trajectory.percent_completion;

        // Joint states
        panda_rundata_publisher.msg_.joint_command_.effort = std::vector<double>(joint_torque_out_.data(), joint_torque_out_.data() + joint_torque_out_.rows() * joint_torque_out_.cols());
        panda_rundata_publisher.msg_.joint_command_.velocity = std::vector<double>(joint_velocity_out_.data(), joint_velocity_out_.data() + joint_velocity_out_.rows() * joint_velocity_out_.cols());
        
        panda_rundata_publisher.unlockAndPublish();
    }

    // publish telemanipulation pose
    if (panda_pose_publisher.trylock()){
        panda_pose_publisher.msg_.header.stamp = ros::Time::now();
        panda_pose_publisher.msg_.header.frame_id = root_link_;
        panda_pose_publisher.msg_.pose = X_curr_msg_;
        panda_pose_publisher.unlockAndPublish();
    }

    // publish wireloop pose
    if (wireloop_pose_publisher.trylock()){
        wireloop_pose_publisher.msg_.header.stamp = ros::Time::now();
        wireloop_pose_publisher.msg_.header.frame_id = root_link_;
        wireloop_pose_publisher.msg_.pose = X_wireloop_msg_;
        wireloop_pose_publisher.unlockAndPublish();
    }

    // publish trajectory pose
    if (trajectory_pose_publisher.trylock()){
        trajectory_pose_publisher.msg_.header.stamp = ros::Time::now();
        trajectory_pose_publisher.msg_.header.frame_id = root_link_;
        trajectory_pose_publisher.msg_.pose = X_traj_msg_;
        trajectory_pose_publisher.unlockAndPublish();
    }
}

bool Controller::updateUI(panda_mover::UI::Request &req, panda_mover::UI::Response &resp)
{
    traj_properties_.play_traj_ = req.play_traj;
    if (req.publish_traj)
        publish_trajectory();
    if (req.build_traj)
        build_trajectory(X_curr_);

    Eigen::Matrix<double, 6, 1> p_gains;
    tf::twistMsgToEigen(req.p_gains_, p_gains);
    p_gains_ = p_gains;
    Eigen::Matrix<double, 6, 1> d_gains;
    tf::twistMsgToEigen(req.d_gains_, d_gains);
    d_gains_ = d_gains;
    traj_properties_.move_ = req.move;
    traj_properties_.gain_tunning_ = req.tune_gain;
    traj_properties_.jogging_ = req.jog_robot;
    traj_properties_.amplitude = req.amplitude;
    traj_properties_.index_ = req.axis;
    virtual_walls = req.virtual_walls;
    resp.result = true;

    if (req.positioning_)
    {
        manual_movement = false;
        selection_matrix_ = Eigen::MatrixXd::Identity(6,6);

        // Check if the next frame is similar to the current frame (Is it safe to proceed?)
        KDL::Twist X_diff = diff(X_curr_, X_traj_);
        ROS_INFO_STREAM("X_diff: " << X_diff << '\t' << X_diff.vel.Norm() << '\t' << X_diff.rot.Norm());
        if (X_diff.vel.Norm() > 0.01 || X_diff.rot.Norm() > 0.05){
            X_traj_ = X_curr_;
            Xd_traj_.Zero();
            Xdd_traj_.Zero();
            must_rebuild = true;
        }
    } 
    else
    {
        manual_movement = true;
        if (test_constraints) selection_matrix_.block(0,0,3,3) = Eigen::MatrixXd::Identity(3,3);
        else selection_matrix_.block(0,0,3,3) = Eigen::MatrixXd::Zero(3,3);
        selection_matrix_.block(3,3,3,3) = Eigen::MatrixXd::Identity(3,3);
        selection_matrix_(5,5) = 0.0;
        std::cout << "selection matrix " << selection_matrix_ <<  std::endl;
    }
    std::cout << "Updating UI \n" << req << std::endl;
    resp.result = true;
}
}
