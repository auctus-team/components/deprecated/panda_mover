#!/usr/bin/env bash

stage_welcome()
{
  prompt_str $1 $2 "Press any key to continue, or Ctrl^C to exit"
  sudo updatedb
}

stage_ibex_install()
{
  print_stage $1 $2 "Installing ibex..."
  sudo apt-get install libmpfr-dev
  sudo sh -c 'echo "https://www.ibex-lib.org/$IBEX.tgz"'
  mkdir -p $DIR_3RD
  cd $DIR_3RD
  if [[ ! -e ibex-lib ]]; then
    git clone https://github.com/ibex-team/ibex-lib.git
    cd ibex-lib
    git fetch && git checkout develop
    ./waf --prefix=./build configure --enable-shared
    ./waf install
  else 
    print_stage $1 $2 "Ibex already installed"
  fi
}

print_bold()
{
    echo -e "$(tput bold)$1$(tput sgr0)"
}

print_stage()
{
  print_bold "\n[$1/$2] $3"
}

prompt_str()
{
    read -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

prompt_str_pass()
{
    read -s -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

echo "###########################"
echo "# INSTALLER               #"
echo "###########################"
echo ""

STAGE_COUNT=1

PROJECT_NAME="Franka-HRI-controller"
FOLDER_START=$(pwd)
DIR_3RD="$FOLDER_START/3rd/"
DIR_BIN="$FOLDER_START/bin/"

stage_welcome
stage_ibex_install 1 $STAGE_COUNT
echo -e "\nInstallation finished !\n"



