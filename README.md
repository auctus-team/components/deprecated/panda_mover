# Continous integration

[![pipeline status](https://gitlab.inria.fr/auctus/panda/panda_mover/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/panda_mover/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:panda_mover)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:panda_mover&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:panda_mover)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Apanda_mover
- Documentation : https://auctus.gitlabpages.inria.fr/panda/panda_mover/index.html

# Installation
1. `sudo apt install python-wstool ros-melodic-vrpn-client-ros`
2. Follow the installation instruction of the fraka ros package : https://frankaemika.github.io/docs/installation_linux.html#installing-from-the-ros-repositories
3. Install the qpOASES ros package in the same catkin workspace: https://github.com/kuka-isir/qpOASES.git
4. Configure a new catkin workspace that extends the previous workspace and sets libfranka path `catkin config --extend ~/catkin_ws/devel/ --cmake-args -DCMAKE_BUILD_TYPE=Release -DFranka_DIR:PATH=/opt/frankaemika/libfranka/build`
5. `cd catkin_ws_mover/src`
6. `git clone git@gitlab.inria.fr:auctus/panda/panda_mover.git`
7. `wstool init`
8. `wstool merge panda_mover/rosdep.rosinstall`
9. `wstool update`
10. `catkin build panda_mover panda_mover_gui`

# Run simulation
`roslaunch panda_mover run.launch sim:=true`

# Calibrate robot and optitrack
Make sure the rigid body markerset is installed on the robot end-effector.

On Narrosse run:
`roslaunch panda_mover run.launch`

To get the differene between the optitrack and robot frames run:
`rosrun panda_mover robotbody_diff`

For manual calibration, adjust the optitrack reference frame to minimize frame differences.

# Experiment procedure
On Narrosse run:
`roslaunch panda_mover run.launch`

On local PC run:
`rosrun panda_mover_gui panda_mover_gui`

1. Comm tab -> Connect. 
2. Main tab -> Play trajectory. Allow robot to move to the start of the wireloop. 
3. Main tab -> Cancel positioning task. 
4. Attach handle to end-effector.
5. Start save data (see below).
6. Human subject performs wireloop task.
7. Stop save data (CTRL-C).

To save data:
`rosbag record /panda_mover/panda_rundata /contact /joint_states /panda_mover/Polyline_pose_array /panda_mover/Pose_array /panda_mover/panda_pose /panda_mover/panda_rundata /panda_mover/trajectory_pose /panda_mover/wireloop_pose /qp_state /tf`

To calibrate the robot and optitrack

