include(FindPackageHandleStandardArgs)

find_path(ibex_INCLUDE_DIR
          NAMES ibex.h
          HINTS "3rd/ibex-lib/build/include"
                ENV ibex_SOURCE_DIR
          PATH_SUFFIXES include)

find_path(ibex_LIB_DIR
             NAMES libibex.so
             HINTS "3rd/ibex-lib/build/lib"
                   ENV ibex3rd_SOURCE_DIR
             PATH_SUFFIXES lib
                           libs)

find_library( ibex_LIBRARY 
              NAMES ibex
              PATHS "3rd/ibex-lib/build/lib"
                   ENV ibex3rd_SOURCE_DIR
                   )

find_library( gaol_LIBRARY  
                NAMES gaol  
                HINTS "3rd/ibex-lib/build/lib/ibex/3rd" 
                ENV ibex3rd_SOURCE_DIR
)

find_library( gdtoa_LIBRARY  
                NAMES gdtoa
                HINTS "3rd/ibex-lib/build/lib/ibex/3rd" 
                ENV ibex3rd_SOURCE_DIR
)

find_library( ultim_LIBRARY  
                NAMES ultim
                HINTS "3rd/ibex-lib/build/lib/ibex/3rd" 
                ENV ibex3rd_SOURCE_DIR
)

find_library( m_LIBRARY  
                NAMES m
)

find_library( mpfr_LIBRARY  
                NAMES mpfr
)

find_library( gmp_LIBRARY  
                NAMES gmp
)
                   
message("m_LIBRARY = ${m_LIBRARY}")
set(ibex_INCLUDE_DIRS ${ibex_INCLUDE_DIR} ${ibex_INCLUDE_DIR}/ibex ${ibex_INCLUDE_DIR}/ibex/3rd)
set(ibex_LIB_DIRS ${ibex_LIB_DIR} ${ibex_LIB_DIR}/ibex/3rd)
# set(ibex_LIBRARIES m mpfr gmp ibex gaol gdtoa ultim)
set(ibex_LIBRARIES ${ibex_LIBRARY} ${mpfr_LIBRARY} ${gmp_LIBRARY} ${ibex_LIBRARY} ${gaol_LIBRARY} ${gdtoa_LIBRARY} ${ultim_LIBRARY} )

find_package_handle_standard_args(ibex DEFAULT_MSG ibex_LIB_DIRS
                                                   ibex_LIBRARIES
                                                   ibex_INCLUDE_DIRS)
set(ibex_FOUND ${ibex_FOUND})
