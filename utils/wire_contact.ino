/*
 * rosserial publisher for wireloop contact
 */

#include <ros.h>
#include <std_msgs/Bool.h>

ros::NodeHandle  nh;

int buzzerOutput = 2;  
int wireOutput = 4;    
int wireInput = 6;      

std_msgs::Bool contact_msg;
ros::Publisher contactPub("contact", &contact_msg);

void setup()
{
  pinMode(buzzerOutput, OUTPUT);
  pinMode(wireOutput, OUTPUT);
  digitalWrite(wireOutput, HIGH);
  pinMode(wireInput, INPUT);
  nh.initNode();
  nh.advertise(contactPub);
}

void loop()
{
  if (digitalRead(wireInput) == HIGH){
    contact_msg.data = true;
    digitalWrite(buzzerOutput, HIGH);
  } else {
    contact_msg.data = false;
    digitalWrite(buzzerOutput, LOW);
  }
  
  contactPub.publish( &contact_msg );
  nh.spinOnce();
  delay(10);
}
