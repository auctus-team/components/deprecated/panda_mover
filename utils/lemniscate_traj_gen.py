import numpy as np
import matplotlib.pyplot as plt
import csv

alpha = 0.1
t = np.linspace(0, 2*np.pi, num=50)

x = 0.4 + alpha * np.sqrt(2) * np.cos(t) * np.sin(t) / (np.sin(t)**2 + 1)
y = alpha * np.sqrt(2) * np.cos(t) / (np.sin(t)**2 + 1)

plt.plot(x, y)
plt.show()

np.savetxt("lemniscate.csv",np.transpose([x,y]),delimiter=",")
